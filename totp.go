package totp

import (
	"crypto/hmac"
	"crypto/sha1"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func CalcTOTP(decimals []int) int {
	// 19th array element bitwise mask for offset
	offset := decimals[19] & 0xf

	b24 := (decimals[offset] & 0x7f) << 24
	b16 := (decimals[offset+1] & 0xff) << 16
	b8 := (decimals[offset+2] & 0xff) << 8
	b2 := decimals[offset+3] & 0xff

	// OR values together
	bor := b24 | b16 | b8 | b2

	// modulus of divide by 10^6 (6 digits)
	modulus := bor % 1000000

	return modulus
}

func HexToDecimals(hx string) ([]int, error) {
	sz := len(hx)
	buf := make([]int, sz/2)
	digits := strings.Split(hx, "")
	n := 0

	for i := 0; i < sz-1; i += 2 {
		data, err := decodeHexPair(digits[i], digits[i+1])
		if err != nil {
			return buf, err
		}

		// cast byte as decimal
		buf[n] = int(data)
		n++
	}

	return buf, nil
}

func CalcHMAC(secret, message []byte) []byte {
	mac := hmac.New(sha1.New, secret)
	mac.Write(message)
	expected := mac.Sum(nil)

	return expected
}

func StampToHex(stamp int) ([]byte, error) {
	// convert stamp to hex
	hx := strconv.FormatInt(int64(stamp), 16)

	// when length less than 16 digits pad left
	// one hexadecimal digit is 4 bits (*16 == 8 bytes)

	buf := make([]byte, 8, 8)
	digits := strings.Split(hx, "")
	i := len(digits) - 1
	n := 7

	// the left-most bytes initialize to zero which is
	// "padding" so start loop from the right side and
	// go left (i.e.,count down)
	for ; i > 0; i -= 2 {
		data, err := decodeHexPair(digits[i-1], digits[i])
		if err != nil {
			return buf, err
		}
		buf[n] = data
		n--
	}

	if i == 0 {
		// loop ended with digits[0] remaining
		data, err := decodeHexPair(digits[0], "")
		if err != nil {
			return buf, err
		}
		buf[n] = data
	}

	return buf, nil
}

func decodeHexPair(digitA, digitB string) (byte, error) {
	// pick the two hexadecimal digits
	pair := digitA + digitB

	// two hexadecimal digits fits one byte
	decoded, err := strconv.ParseInt(pair, 16, 16)
	if err != nil {
		return 0, errors.Wrap(err, "parse hex digits failed at " + digitA)
	}
	return byte(decoded), nil
}

func CountIntervals(stamp string) (int, error) {
	var total int
	interval := 30

	if len(stamp) == 0 {
		// Calculate the number of 30s intervals since epoch
		st := time.Now().Unix() / int64(interval)
		total = int(st)
	} else {
		dur, err := time.ParseDuration(stamp + "s")
		if err != nil {
			return 0, errors.Wrap(err, "parse stamp as seconds failed")
		}
		st := dur.Truncate(time.Second).Seconds() / float64(interval)
		total = int(st)
	}

	return total, nil
}
