package totp

import (
	"bytes"
	"testing"
	"time"
)

func TestCountIntervals(t *testing.T) {
	var stamp = "1331283521.00001"
	var interval = 30
	result, _ := CountIntervals(stamp)

	if result != 44376117 {
		t.Error("Expected the half-minutes 44376117, got ", result)
	}

	// Number of 30s intervals since epoch
	count := time.Now().Unix() / int64(interval)
	result, _ = CountIntervals("")
	if result != int(count) {
		t.Errorf("Expected the # of half-mins to be %d, got %d", count, result)
	}

	// Trigger error with bad stamp format
	_, err := CountIntervals("h")
	if err == nil {
		t.Error("Expected malformed stamp error, got silence")
	}
}

func TestStampToHex(t *testing.T) {
	count := 44376117
	result, _ := StampToHex(count)

	expval := []byte{0, 0, 0, 0, 2, 165, 32, 53}
	if !bytes.Equal(result, expval) {
		t.Errorf("Expected hex %x, got %x", expval, result)
	}

	// hexadecimal of size 2
	count = 255
	result, _ = StampToHex(count)
	expval = []byte{0, 0, 0, 0, 0, 0, 0, 255}
	if !bytes.Equal(result, expval) {
		t.Errorf("Expected hex %x, got %x", expval, result)
	}
	
	// hexadecimal of size 1
	count = 8
	result, _ = StampToHex(count)
	expval = []byte{0, 0, 0, 0, 0, 0, 0, 8}
	if !bytes.Equal(result, expval) {
		t.Errorf("Expected hex %x, got %x", expval, result)
	}

	// hexadecimal of size ~16
	count = 999999999999999999
	result, _ = StampToHex(count)
	expval = []byte{13, 224, 182, 179, 167, 99, 255, 255}
	if !bytes.Equal(result, expval) {
		t.Errorf("Expected hex %x, got %x", expval, result)
	}
}

func TestCalcHMAC(t *testing.T) {
	secret := []byte{49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48}
	message := []byte{0, 0, 0, 0, 2, 165, 32, 53}

	result := CalcHMAC(secret, message)
	expval := []byte{175, 43, 136, 4, 141, 200, 151, 155, 82, 138, 244, 227, 112, 133, 6, 29, 136, 170, 170, 165}
	if !bytes.Equal(result, expval) {
		t.Errorf("Expected HMAC %x, got %x", expval, result)
	}
}

func TestHexToDecimals(t *testing.T) {
	hx := "AF2B88048DC8979B528AF4E37085061D88AAAAA5"

	result, _ := HexToDecimals(hx)

	expval := []int{175, 43, 136, 4, 141, 200, 151, 155, 82, 138, 244, 227, 112, 133, 6, 29, 136, 170, 170, 165}

	if notEqual(result, expval) {
		t.Errorf("Expected decimals %v, got %v", expval, result)
	}

}

func TestCalcTOTP(t *testing.T) {
	decimals := []int{175, 43, 136, 4, 141, 200, 151, 155, 82, 138, 244, 227, 112, 133, 6, 29, 136, 170, 170, 165}

	result := CalcTOTP(decimals)
	expval := 895250

	if result != expval {
		t.Errorf("Expected modulus %v, got %v", expval, result)
	}
}

func notEqual(x, y []int) bool {
	if len(x) != len(y) {
		return true
	}
	for k, v := range x {
		if y[k] != v {
			return true
		}
	}
	return false
}


