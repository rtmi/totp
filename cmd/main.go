package main

import (
	"encoding/base32"
	"encoding/hex"
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/patterns/totp"
)

func main() {
	var key = flag.String("key", "", "Secret key e.g., GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ")
	var stamp = flag.String("stamp", "", "Unix time stamp e.g., 1331283521.00001")

	flag.Parse()

	var secret = strings.ToUpper(*key)
	if *key == "" {
		kenv, ok := os.LookupEnv("TOTP_SECRET")
		if !ok {
			panic("A secret key is required. Use -key commandline arg, or env var TOTP_SECRET")
		}
		secret = strings.ToUpper(kenv)
	}

	sdata, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		panic(err)
	}

	st, err := totp.CountIntervals(*stamp)
	if err != nil {
		panic(err)
	}

	message, err := totp.StampToHex(st)
	if err != nil {
		panic(err)
	}

	expected := totp.CalcHMAC(sdata, message)

	encodedStr := hex.EncodeToString(expected)

	decimals, err := totp.HexToDecimals(encodedStr)
	if err != nil {
		panic(err)
	}

	modulus := totp.CalcTOTP(decimals)
	fmt.Println(modulus)
}
