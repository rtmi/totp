# totp
Calculate value to match Google Authenticator.
 This is a Golang version of the steps explained by Vyacheslav in his article
 [Implementing Google's Two-Step Auth] (https://www.codementor.io/slavko/google-two-step-authentication-otp-generation-du1082vho)

## Credits

[Implementing Google's Two-Step Auth] (https://www.codementor.io/slavko/google-two-step-authentication-otp-generation-du1082vho)

